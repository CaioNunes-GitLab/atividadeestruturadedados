package br.com.ucsal.edd;

public class ListaSimples {

    No inicio = null;
    int tamanho = 0;


    public void inserirElementoInicio(String info){
        if (!verificaLista()) {
            No no = new No();
            no.info = info;
            no.proximo = inicio;
            inicio = no;
            tamanho++;
        }
    }

    public void inserirElementoFim(String info){
        if(!verificaLista()) {
            No no = new No();
            no.info = info;
            if (inicio == null) {
                no.proximo = null;
                inicio = no;
            } else {
                No local = inicio;
                while (local.proximo != null) {
                    local = local.proximo;
                }
                local.proximo = no;
                no.proximo = null;
            }
        }
    }
    public Boolean verificaLista(){
        if(tamanho >= 7){
            System.out.println("\n-------Lista cheia, sua adição não foi inserida-------");
            return true;
        }
        return false;
    }

    public String toString(){
        String str = " ( " + tamanho +" ) ";
        No local = inicio;
        while(local != null){
            str += local.info + " ";
            local = local.proximo;
        }
        return str;
    }
}
