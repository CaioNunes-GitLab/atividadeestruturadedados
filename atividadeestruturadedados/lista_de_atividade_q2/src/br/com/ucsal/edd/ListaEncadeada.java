package br.com.ucsal.edd;

public class ListaEncadeada {
    private Matricula inicio = null;
    private int tamanho = 0;

    String infoAluno;

    public void setInfoAluno(String infoAluno) {
        this.infoAluno = infoAluno;
    }

    public String getInfoAluno() {
        return infoAluno;
    }

    public ListaEncadeada() {
    }

    public void inserirInicio(String info) {
        Matricula no = new Matricula();
        no.listaEncadeada.setInfoAluno(info);
        no.proximo = inicio;
        inicio = no;
        tamanho++;
    }

    public void inserirPosicao(int posicao, String info) {
        if (posicao <= 0) {//verifica lista vazia
            inserirInicio(info);
        } else if (posicao >= tamanho) {//se a posição for maior ou igual que o tamanho, quer dizer que não há posições suficientes e elas serão add no fim
            inserirFim(info);
        } else {
            Matricula local = inicio;
            for (int i = 0; i < posicao - 1; i++) {
                local = local.proximo;
            }
            Matricula no = new Matricula();
            no.listaEncadeada.setInfoAluno(info);//o no guarda a info
            no.proximo = local.proximo;//o proximo desse no vai estar apontando para o local.proximo
            local.proximo = no;//local.proximo agora é o no
            tamanho++;//lista aumenta :)
        }

    }

    public void inserirFim(String info) {
        Matricula no = new Matricula();
        no.listaEncadeada.setInfoAluno(info);
        if (inicio == null) {
            inicio = no;//inicio pro novo no
            no.proximo = null;//o proximo nó que é null agora
        } else {
            Matricula local = inicio;
            while (local.proximo != null) {
                local = local.proximo;
            }
            local.proximo = no;
            no.proximo = null;
        }
        tamanho++;
    }

    public String toString() {
        String str = "(" + tamanho + ") ";
        Matricula local = inicio;//variavel aux para pegar elemento e o proximo pra mostrar na tela
        while (local != null) {
            str += local.listaEncadeada.getInfoAluno() + " ";
            local = local.proximo;
        }
        return str;
    }

    public String removerPosicao(int posicao) {
        Matricula no = new Matricula();
        String info = inicio.listaEncadeada.getInfoAluno();
        if (inicio == null || posicao < 0 || posicao >= tamanho) {
            return info;
        } else if (posicao == 0) {
            return retirarInicio();
        } else if (posicao == tamanho - 1) {
            return retirarFim();
        }
        Matricula local = inicio;
        for (int i = 0; i < posicao - 1; i++) {
            local = local.proximo;
        }
        info = (String) local.proximo.listaEncadeada.getInfoAluno();
        local.proximo = local.proximo.proximo;
        tamanho--;
        return info;
    }

    public String retirarFim() {
        Matricula local = inicio;
        while (local.proximo.proximo != null) {
            local = local.proximo;
        }
        local.proximo = null;
        tamanho--;
        return null;
    }

    public String retirarInicio() {
        if (inicio == null) {
            return null;
        }
        String info = inicio.listaEncadeada.getInfoAluno();
        inicio = inicio.proximo;
        tamanho--;
        return info;
    }

    public String buscarItem(int indice) {
        if (inicio == null) {
            return "Não há nenhum aluno na lista";
        }
        Matricula local = inicio;
        int aux = 1;
        while (local != null) {
            if (aux == indice) {
                return "Indice: " + aux + ", Aluno: " + local.listaEncadeada.getInfoAluno();
            }
            local = local.proximo;
            aux++;
        }
        return "Indice correspondente ao aluno não encontrado";
        //Hello World
    }

    public void ordenacaoOrdemAlfabetica() {
        Matricula local = inicio;
        
        while(local != null){
            Matricula localProximo = local.proximo;
            while(localProximo != null){
                if(local.listaEncadeada.getInfoAluno().compareToIgnoreCase(localProximo.listaEncadeada.getInfoAluno()) > 0){
                    ListaEncadeada aux = local.listaEncadeada;
                    local.listaEncadeada = localProximo.listaEncadeada;
                    localProximo.listaEncadeada = aux;
                }
                localProximo = localProximo.proximo;
            }
            local = local.proximo;
        }
    }
}



