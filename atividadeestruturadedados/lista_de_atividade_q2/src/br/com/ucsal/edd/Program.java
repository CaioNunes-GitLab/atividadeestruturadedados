package br.com.ucsal.edd;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ListaEncadeada lista = new ListaEncadeada();
        int resp = 0;
        System.out.println("Bem-vindo(a) ao sistema de matrícula de alunos! Escolha dentre as opções:");
        do {
            System.out.println(
                    "\n(1) Adicionar Aluno" +
                    "\n(2) Remover Aluno" +
                    "\n(3) Pesquisar Aluno" +
                    "\n(4) Apresentar lista em ordem alfabética" +
                    "\n(5) Sair");
            resp = sc.nextInt();
            switch (resp) {
                case 1: {
                    System.out.println("Informe o nome do aluno: ");
                    sc.nextLine();
                    String nomeAluno = sc.nextLine();
                    lista.inserirFim(nomeAluno);
                    System.out.println("Aluno(a) matriculado com sucesso: " + nomeAluno);
                    break;
                }
                case 2: {
                    System.out.println(lista);
                    System.out.println("Informe o índice correspondente ao aluno na lista: ");
                    int indice = sc.nextInt();
                    System.out.println("Retirado(a) da lista: " + lista.removerPosicao(indice));
                    break;
                }
                case 3: {
                    System.out.println(lista);
                    System.out.println("Informe o índice correspondente ao aluno na lista: ");
                    int i = sc.nextInt();
                    System.out.println("Resultado: " + lista.buscarItem(i));
                    break;
                }
                case 4: {
                    lista.ordenacaoOrdemAlfabetica();
                    System.out.println(lista);
                    break;

                }
            }
        } while (resp != 5);

    }
}
