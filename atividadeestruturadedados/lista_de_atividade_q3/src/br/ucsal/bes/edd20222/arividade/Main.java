package br.ucsal.bes.edd20222.arividade;

import br.ucsal.bes.edd20222.arividade.util.Lista;

public class Main {
    public static void main(String[] args) {
        Lista lista = new Lista();

        System.out.println(lista.inserirFinal(15));

        System.out.println(lista.listarFull());

        System.out.println(lista.listarPos());
    }
}
