package br.ucsal.bes.edd20222.arividade.util;

import java.util.Scanner;

public class Lista {
    No inicio = null;
    public int tamanho = 0;

    public String inserirInicio() {
        No no = new No();
        no.info = (int)(Math.random()*(999));
        no.proximo = inicio;
        inicio = no;
        tamanho++;

        return no.info+" adicionado.";
    }

    public String inserirFinal(int qtd) {
        if (qtd==1) {
            return "Pronto!\n";
        }

        if (inicio==null) {
            System.out.println(inserirInicio());
        }

        No no = new No();
        no.info = (int)(Math.random()*(999));
        no.proximo = null;
        No local = inicio;
        while(local.proximo!=null) {
            local = local.proximo;
        }
        local.proximo = no;
        tamanho++;
        System.out.println(no.info+" adicionado.");

        return inserirFinal(qtd-1);
    }

    public String listarFull() {
        if (inicio==null) {
            return "Lista vazia";
        }
        No local = inicio;
        if (local.proximo!=null) {
            int i=2;
            return "1. " + local.info +"\n" +listarFull(local.proximo, i);
        } else {
            return "1. " + local.info +"\n";
        }
    }

    public String listarFull(No local, int i) {
        if (local.proximo!=null) {
            return i+". " + local.info +"\n" +listarFull(local.proximo, i+1);
        } else {
            return i+". " + local.info +"\n";
        }
    }


    public String listarPos() {
        if (inicio == null) {
            return "Lista vazia";
        }
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite o número desejado");
        int info = sc.nextInt();
        No local = inicio;
        int i = 1;
        while (local.proximo != null && local.info != info) {
            local = local.proximo;
            i++;
        }
        if (local.info == info){
            return i + ". " + local.info;
        } else {
            System.out.println("Item não encontrado\n");
            return listarPos();
        }
    }
}
